/*
 * TestingCapture.cpp
 *
 *  Created on: May 26, 2015
 *      Author: servio
 */

#include <stdint.h>
#include "gtest/gtest.h"
#include "../CamCapture/CamCapture.h"

using namespace cv;

class TestCapture: public ::testing::Test {
protected:
	virtual void SetUp() {

	}
	virtual void TearDown() {

	}

	Mat image;
	CamCapture camCapture;
	uchar returnValue;
};

TEST_F(TestCapture, ReadPhotoFromFile) {
	char file[] = "gradient.png";
	int status = camCapture.takePhotoFromFile(file, &image);
	ASSERT_EQ(status, CAMCap_SUCCESS);
}

TEST_F(TestCapture, ReadValue) {
	char file[] = "gradient.png";
	int lecture = camCapture.takePhotoFromFile(file, &image);
	int status = camCapture.readValue(&image, &returnValue, 0, 0);

	ASSERT_EQ(lecture, CAMCap_SUCCESS);
	ASSERT_EQ(status, CAMCap_SUCCESS);
	ASSERT_EQ(0, returnValue);
}
