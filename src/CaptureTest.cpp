//============================================================================
// Name        : CaptureTest.cpp
// Author      : Servio Paguada
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <gtest/gtest.h>

int main(int narg, char *args[]) {
	testing::InitGoogleTest(&narg, args);

	return RUN_ALL_TESTS();
}
